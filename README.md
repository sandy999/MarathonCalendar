This project is for demonstrational purposes and on a couple of modifications for personal use only.

Welcome to Marathon Calendar!

If you are a regular Indian marathoner, you might be searching on indiarunning.com trying to find the next few marathons occuring in your city as per your category depending your fitness.

But what if you received text notifications on your mobile phone along with emails about the marathons happening in your city as per your category in the next 3 months. Why 3 months? That's usually when registrations for the marathons and their associated training sessions begin.

First off, to set the city and category of marathons you want to filter by (initially and later, if you want to change)

Start the rot13.service (for webscraper2.py, don't worry about the name) and marathonCalendarServer.service

-Open localhost:5000 on your browser
-Click on "Edit settings"
-Enter city and category you want to filter by, time and days of the week you want to be reminded through sms and submit

That's it. When it's time to remind you, you will get texts and emails about the upcoming marathons
