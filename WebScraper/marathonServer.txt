[Unit]
Description=Delta task, keep server always on
After=network.target
After=mysqld.service
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
User=sandy
ExecStart=/home/sandy/anaconda3/envs/my_env/bin/python /home/sandy/Project/WebScraper/server.py
ExecRestart=/home/sandy/anaconda3/envs/my_env/bin/python /home/sandy/Project/WebScraper/server.py

[Install]
WantedBy=multi-user.target



