from flask import Flask, flash, redirect, render_template, request, session, url_for
import webscraping2
import pymysql
from datetime import date
import datetime
# from cs50 import SQL
# from flask_session import Session

def day(number):
    return 1 << number

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("layout.html")

@app.route("/settings",methods=["GET","POST"])
def settings():
    if request.method == 'POST':
        db = pymysql.connect("localhost", "phpmyadmin", "", "marathon")
        cursor = db.cursor()
        propercityfilter = request.form.get('cityfilter')
        propercategoryfilter = request.form.get('categoryfilter')
        propertime = request.form.get('timefilter')
        # print(propercityfilter)
        # print(propercategoryfilter)
        selected_days = request.form.getlist('day')
        bitday = 0
        for item in selected_days:
            if item=="Monday":
                bitday = bitday | day(0)
            elif item=="Tuesday":
                bitday = bitday | day(1)
            elif item=="Wednesday":
                bitday = bitday | day(2)
            elif item=="Thursday":
                bitday = bitday | day(3)
            elif item=="Friday":
                bitday = bitday | day(4)
            elif item=="Saturday":
                bitday = bitday | day(5)
            elif item=="Sunday":
                bitday = bitday | day(6)
        # print(bitday)
        numofrows = cursor.execute("SELECT* FROM marathonCalendar")
        if numofrows==0:
            cursor.execute("INSERT INTO marathonCalendar (city,category,day,time) VALUES ('%s','%s','%d','%s')" % (propercityfilter,propercategoryfilter,bitday,propertime))
        else:
            cursor.execute("UPDATE marathonCalendar SET city='%s',category='%s',day='%d',time='%s'" % (propercityfilter,propercategoryfilter,bitday,propertime))
        db.commit()
        db.close()
        return redirect(url_for("home"))
    else:
        return render_template("settings.html")


if __name__=="__main__":
    app.run(debug=True,host='0.0.0.0')
    
