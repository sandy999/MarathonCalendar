from bs4 import BeautifulSoup
import requests
import re
from datetime import date
import datetime
from twilio.rest import Client
import pymysql

def smsinfoinit(text):
    accountSID='AC25fa9d151afafcb75ad66e627cd78db0'
    authToken = 'de2ebc49a13c3cd9b94d8006f2b39566'
    twilioCli = Client(accountSID,authToken)
    myTwilioNumber = '+18506607286'
    myCellPhone = '+919840580366'
    message = twilioCli.messages.create(body=text,from_=myTwilioNumber,to=myCellPhone)

def scrape(cityfilter,categoryfilter):
    page = requests.get("http://indiarunning.com/marathon-calendar.html")
    soup = BeautifulSoup(page.content, 'html.parser')
    for match in soup.find_all('font'):# removes all font tags
        match.unwrap()
    # print(soup)
    dates = soup.find_all('td',text=re.compile('\d\d-\w\w\w-\d\d\d\d'))# extracts only the td whose contents are of the date format
    for dateitem in dates:
        datetext = dateitem.get_text()# Stores data in text form
        eventtag = dateitem.findNext('td')
        link = eventtag.find('a').get('href')
        eventtext = eventtag.get_text()
        citytag = eventtag.findNext('td')
        citytext = citytag.get_text()# Stores in the form (city, state). I just want city so I need to split over the , check next line
        citytext = citytext.split(', ')[0]
        categorytag = citytag.findNext('td')
        categorytext = categorytag.get_text()# Stores the list of categories Half 10K 5K etc in text form
        categorytext = " ".join(categorytext.split())# remember the category listing text had unnecessary extra spaces and newlines, so I split and rejoined
        categorylist = categorytext.split(' ')# Stores like [10K, 5K, 3K] so that I can access each category using list index
        k = datetext.split('-')#Splits into list containing date, month, year as elements
        # print(k)
        global d1
        d0 = date(datetime.datetime.now().year, datetime.datetime.now().month, datetime.datetime.now().day)
        if k[1] == 'Jan':# All this to convert to date (timedelta) format so that I can subtract from current date and find interval
            d1 = date(int(k[2]), 1, int(k[0]))
        elif k[1] == 'Feb':
            d1 = date(int(k[2]), 2, int(k[0]))
        elif k[1] == 'Mar':
            d1 = date(int(k[2]), 3, int(k[0]))
        elif k[1] == 'Apr':
            d1 = date(int(k[2]), 4, int(k[0]))
        elif k[1] == 'May':
            d1 = date(int(k[2]), 5, int(k[0]))
        elif k[1] == 'Jun':
            d1 = date(int(k[2]), 6, int(k[0]))
        elif k[1] == 'Jul':
            d1 = date(int(k[2]), 7, int(k[0]))
        elif k[1] == 'Aug':
            d1 = date(int(k[2]), 8, int(k[0]))
        elif k[1] == 'Sep':
            d1 = date(int(k[2]), 9, int(k[0]))
        elif k[1] == 'Oct':
            d1 = date(int(k[2]), 10, int(k[0]))
        elif k[1] == 'Nov':
            d1 = date(int(k[2]), 11, int(k[0]))
        elif k[1] == 'Dec':
            d1 = date(int(k[2]), 12, int(k[0]))
        delta = d1 - d0# No. of days ahead of day, the marathon is gonna be held, filter out those occurring within 3 months
        # print("No of days: " + str(delta.days))
        if int(str(delta.days))<=90 and int(str(delta.days))>0 and cityfilter=="All" and categoryfilter=="All":# Lists all marathons
            # f.write(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
            smsinfoinit(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
        elif int(str(delta.days))<=90 and int(str(delta.days))>0 and cityfilter=="All":# If only category mentioned explicitly, filters based on city
            flag = 0
            for item in categorylist:
                if item == categoryfilter:
                    flag = 1
                    break
            if flag==1:
                # f.write(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
                smsinfoinit(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
        elif int(str(delta.days))<=90 and int(str(delta.days))>0 and categoryfilter=="All":# If only city mentioned explicitly, filters based on category
            if citytext==cityfilter:
                # f.write(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
                smsinfoinit(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
        elif int(str(delta.days))<=90 and int(str(delta.days))>0:# Filter based on mentioned city and category
            flag = 0
            for item in categorylist:
                if item==categoryfilter:
                    flag=1
                    break
            if flag==1 and citytext==cityfilter:
                # f.write(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
                smsinfoinit(datetext + "\n" + link + "\n" + eventtext + "\n" + citytext + "\n" + categorytext + "\n\n")
    # f.close()
    # print(categorytext)
    # print(categorylist)
    # print(cityfilter)
    # print(categoryfilter)

if __name__=='__main__':
    while 1:
        currentday = datetime.datetime.today().weekday()
        currentday = 1 << currentday
        db = pymysql.connect("localhost", "phpmyadmin", "", "marathon")
        cursor = db.cursor()
        cursor.execute("SELECT * FROM marathonCalendar")
        row = cursor.fetchone()
        temp = row[3]
        givenmin = temp % 100
        temp /= 100
        givenhour = int(temp)
        if (currentday & row[2]) and datetime.datetime.now().hour == givenhour and datetime.datetime.now().minute == givenmin:
            cursor.close()
            db.close()
            scrape(row[0], row[1])